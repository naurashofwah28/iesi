<!DOCTYPE html>
<html>

<head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link href="{{ asset('css/regis.css') }}" rel="stylesheet" type="text/css">
</head>
<body>




<div class="medclinic">myClinic</div>
<div class="image3">
    <img src="img/er.png" alt="image3">
</div>
<h1>Register</h1>
<form method="POST" action="{{ route('register') }}">
    @csrf
    <table>
        <tr>
            <td>
                <label for="name">Name</label>
            </td>
            <td>
                <input type="text" name="name" id="name" value="{{ old('name') }}" class="input">
                @error('name')
                    <span>{{ $message }}</span>
                @enderror
            </td>
        </tr>
        <tr>
            <td>
                <label for="email">Email</label>
            </td>
            <td>
                <input type="text" name="email" id="email" value="{{ old('email') }}" class="input">
                @error('email')
                    <span>{{ $message }}</span>
                @enderror
            </td>
        </tr>
        <tr>
            <td>
                <label for="password">Password</label>
            </td>
            <td>
                <input type="password" name="password" id="password" class="input">
                @error('password')
                    <span>{{ $message }}</span>
                @enderror
            </td>
        </tr>
        <tr>
            <td>
                <label for="password_confirmation">Confirm Password</label>
            </td>
            <td>
                <input type="password" name="password_confirmation" id="password_confirmation" class="input">
                @error('password_confirmation')
                    <span>{{ $message }}</span>
                @enderror
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <button class="button register-button">Register</button>
            </td>
        </tr>
    </table>
</form>

<div class="home">
    <a href='/' class="button " style="color: black">Home</a>
</div>
<div class="home1">
   
    <a href="register" class="button signup-button" style="color: #45a0c7;">Sign Up</a>
    <a href="login" class="button login-button">Log In</a>
   

</div>


  <div class="image">
    <img src="img/imageregis.png" alt="image" >
  </div>
    <div class="image2">
    <img src="img/image-14.png" alt="image" width="100" height="189">
    </div>
</div>
</body>
</html>