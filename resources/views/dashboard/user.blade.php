
<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link href="{{ asset('css/user.css') }}" rel="stylesheet" type="text/css">
    
</head>
<body>
    <div class="image">
        <img src="img/image-13.png" alt="image" width="700px" height="808px">
    </div>
    <div class="image2">
        <img src="img/image-14.png" alt="image" width="112" height="189">
    </div>
    <div class="medclinic">myClinic</div>
    

    <div class="home">
        <a href="/user">Home</a>
        <a href="/antrian">Antrian</a>
        <a href="/index">Lihat Antrian</a>
    </div>
   
    
    <div class="content">
        @auth
        <div class="welcome">Welcome, {{ Auth::user()->name }}</div>
    @endauth
        <div class="meet">
            <p>Meet the Best Medical Clinic</p>
        </div>
        <div class="silahkan">
            <p>Silahkan menekan tombol di bawah ini untuk       mendapatkan nomor Antrian </p>
  
        </div>
        <div class="tombol-submit">
            <a href="/antrian" >AMBIL ANTRIAN ANDA</a>
            
        </div class="logout">
        <a class="btn-logout" href="{{ route('logout') }}" >Log Out</a>
    </div>
  
</body>
</html>
