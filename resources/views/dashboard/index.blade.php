<!DOCTYPE html>
<html>
<head>
    <title>Daftar Antrian</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/index.css') }}">
</head>
<body>
    <h1>Daftar Antrian</h1>
    <div class="home">
        <a href="/user">Home</a>
        <a href="/antrian">Antrian</a>
        <a href="/index">Lihat Antrian</a>
    </div>
    <div class="medclinic">myClinic</div>

    @if ($antrian && $antrian->count() > 0)
        <table>
            <thead>
                <tr>
                    <th>No.</th>
                    <th>No. Antrian</th>
                    <th>Nama</th>
         
                    <th>Kategori</th>
                    <th>Poli</th>
           
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @php $counter = 1 @endphp
                @foreach ($antrian as $data)
                    <tr>
                        <td>{{ $counter }}</td>
                        <td>{{ $data->noAntrian }}</td>
                        <td>{{ $data->nama }}</td>
                
                        <td>{{ $data->kategori }}</td>
                        <td>{{ $data->poli }}</td>
                      
                        <td>{{$data->status}}</td>
                    </tr>
                    @php $counter++ @endphp
                @endforeach
            </tbody>
        </table>
    @else
        <p>Tidak ada data antrian.</p>
    @endif

    <div class="image2">
        <img src="img/image-14.png" alt="image" width="100" height="189">
        </div>
    
</body>
</html>
